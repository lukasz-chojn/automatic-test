Feature: Opening the page and going through the page elements

  Scenario: Main Page
    Given I open the page
    Then I am on the site 'Strona główna'

  Scenario: News Page
    Given I am on the site 'Strona główna'
    When I click on the item named 'Aktualności'
    Then I am on the site 'Aktualności'

  Scenario: About us Page
    Given I am on the site 'Aktualności'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'O Nas'
    Then I am on the site 'O Nas'

  Scenario: TPD activity Page
    Given I am on the site 'O Nas'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'Działalność TPD'
    Then I am on the site 'Działalność TPD'

  Scenario: TPD Daily Support Centers
    Given I am on the site 'Działalność TPD'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'Placówki Wsparcia Dziennego TPD'
    Then I am on the site 'Placówki Wsparcia Dziennego TPD'

  Scenario: TPD facilities addresses
    Given I am on the site 'Placówki Wsparcia Dziennego TPD'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'Placówki TPD adresy'
    Then I am on the site 'Placówki TPD adresy'

  Scenario: Projects
    Given I am on the site 'Placówki TPD adresy'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'Projekty'
    Then I am on the site 'Projekty'

  Scenario: Statute of TPD
    Given I am on the site 'Projekty'
    When I click on the drop-down menu on the value 'TPD' and I choose a link 'Statut TPD'
    Then I am on the site 'Statut TPD'

  Scenario: Spokesman Page
    Given I am on the site 'Statut TPD'
    When I click on the item named 'Rzecznik'
    Then I am on the site 'Rzecznik'

  Scenario: Our friends
    Given I am on the site 'Rzecznik'
    When I click on the drop-down menu on the value 'Przyjaciele' and I choose a link 'Nasi Przyjaciele'
    Then I am on the site 'Nasi Przyjaciele'

  Scenario: Become our friend
    Given I am on the site 'Nasi Przyjaciele'
    When I click on the drop-down menu on the value 'Przyjaciele' and I choose a link 'Zostań Naszym Przyjacielem'
    Then I am on the site 'Zostań Naszym Przyjacielem'

  Scenario: They write about us Page
    Given I am on the site 'Zostań Naszym Przyjacielem'
    When I click on the item named 'Piszą o nas'
    Then I am on the site 'Piszą o nas'

  Scenario: Gallery Page
    Given I am on the site 'Piszą o nas'
    When I click on the item named 'Galeria'
    Then I am on the site 'Galeria'

  Scenario: Contact Page
    Given I am on the site 'Galeria'
    When I click on the item named 'Kontakt'
    Then I am on the site 'Kontakt'

  Scenario: ZO TPD Page
    Given I am on the site 'Kontakt'
    When I click on the item named 'ZO TPD'
    Then I am on the site 'ZO TPD'

  Scenario: Documents Page
    Given I am on the site 'ZO TPD'
    When I click on the drop-down menu on the value 'Wypoczynek' and I choose a link 'Dokumenty'
    Then I am on the site 'Dokumenty'

  Scenario: Colonies
    Given I am on the site 'ZO TPD'
    When I click on the drop-down menu on the value 'Wypoczynek' and I choose a link 'Kolonie'
    Then I am on the site 'Kolonie'