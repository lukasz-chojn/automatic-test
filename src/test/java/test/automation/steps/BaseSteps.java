package test.automation.steps;

import net.thucydides.core.annotations.Step;
import test.automation.pages.Page;

/**
 * Steps for Gherkin steps definition
 */
public class BaseSteps extends Page {

    @Step
    public void openPage() {
        super.open();
    }

    @Override
    @Step
    public void verifyPage(String webpageName) {
        super.verifyPage(webpageName);
    }

    @Override
    @Step
    public void clickOnElement(String elementName) {
        super.clickOnElement(elementName);
    }

    @Override
    @Step
    public void clickDropDownMenuElement(String menuName, String pageName) {
        super.clickDropDownMenuElement(menuName, pageName);
    }
}