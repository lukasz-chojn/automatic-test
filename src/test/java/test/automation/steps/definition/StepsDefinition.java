package test.automation.steps.definition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import test.automation.steps.BaseSteps;

/**
 * Step definition class
 */
public class StepsDefinition {

    @Steps
    private BaseSteps baseSteps;

    @Given("I open the page")
    public void openPage() {
        baseSteps.openPage();
    }

    @When("I click on the item named {string}")
    public void clickOnElement(String element) {
        baseSteps.clickOnElement(element);
    }

    @Then("I am on the site {string}")
    public void verifyPage(String webpageName) {
        baseSteps.verifyPage(webpageName);
    }

    @And("I click on the drop-down menu on the value {string} and I choose a link {string}")
    public void dropDownMenu(String element, String pageName) {
        baseSteps.clickDropDownMenuElement(element, pageName);
    }
}
