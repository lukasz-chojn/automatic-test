package test.automation.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

/**
 * Main PageObject for testing
 */
public abstract class Page extends PageObject {

    public void verifyPage(String webpageName) {
        getDriver().findElement(By.xpath("//*[text()='" + webpageName + "']"));
    }

    public void clickOnElement(String elementName) {
        getDriver().findElement(By.partialLinkText(elementName)).click();
    }

    public void clickDropDownMenuElement(String menuName, String pageName) {
        Actions dropDown = new Actions(getDriver());
        getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        dropDown.moveToElement(getDriver().findElement(By.linkText(menuName))).perform();
        getDriver().findElement(By.linkText(pageName)).click();
    }

    public void table(String columnName, String word) {
        getDriver().findElement(By.xpath("//table[text()[contains(.," + columnName + ")]]"));
        getDriver().findElement(By.xpath("//td[text()[contains(.," + word + ")]]"));
    }
}
