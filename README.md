# Automated test
### Overview
Automated test written in Serenity and Cucumber frameworks. The screen operations are performed by Selenium WebDriver.  
The application can work with following browsers: Google Chrome, Mozilla Firefox, Microsoft Edge and Microsoft Internet Explorer.
### Running application
The application can be run using the command `mvn verify` or by using the main application class named *CucumberTestApp*. 
To run the test, pass the path to the browser driver in the startup parameters. You can do it with the -D parameter, e.g. for Google Chrome it looks like `-Dwebdriver.chrome.driver=path/to/file/driver`. The full run command will be as follows: `mvn verify -Dwebdriver.chrome.driver=path/to/file/driver`. Drivers for the most popular browsers are included with the project.
### Configuration
The entire configuration of the project is saved in the  **serenity.properties** file. You can configure the driver there, enter the url of your website that will be tested and much more. Then adapt the application code to your needs.
### Reporting
The application generates test reports. You can find them in the *target/site/serenity* folder.
### Known issues
If you see the error `Caused by: net.thucydides.core.webdriver.DriverConfigurationError: Could not instantiate new WebDriver instance of type class org.openqa.selenium.browser.BrowserDriver (unknown error: cannot find Browser Name binary)` when starting the application set the variable `webdriver.BrowserName.binary`, e.g. for Google Chrome the variable is: *webdriver.chrome.binary*. The value of the variable is the location of the browser executable file, e.g. for Google Chrome the location can be: `C:\Program Files\Google\Chrome\Application\chrome.exe`. The full entry should look like this: `webdriver.chrome.binary=C:\Program Files\Google\Chrome\Application\chrome.exe`
Place the above entry in the **serenity.properties** file.